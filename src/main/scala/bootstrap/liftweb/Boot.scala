package bootstrap.liftweb

import _root_.net.liftweb.common._
import _root_.net.liftweb.util._
import _root_.net.liftweb.http._
import _root_.net.liftweb.http.provider._
import _root_.net.liftweb.sitemap._
import _root_.net.liftweb.sitemap.Loc._
import Helpers._
import _root_.net.liftweb.mapper.{DB, DBLogEntry, ConnectionManager, Schemifier, DefaultConnectionIdentifier, StandardDBVendor}
import _root_.net.liftweb.mapper.view._
import _root_.net.dataminer.model._
import _root_.net.liftweb.http.js.jquery.JQuery14Artifacts

/**
  * A class that's instantiated early and run.  It allows the application
  * to modify lift's environment
  */
class Boot {
  def boot {
    if (!DB.jndiJdbcConnAvailable_?) {
      val vendor =
	new StandardDBVendor(
          Props.get("db.driver") openOr "org.h2.Driver",
	  Props.get("db.url") openOr 
	  "jdbc:h2:lift_proto.db;AUTO_SERVER=TRUE",
	  Props.get("db.user"), 
          Props.get("db.password"))
      LiftRules.unloadHooks.append(vendor.closeAllConnections_! _)
      DB.defineConnectionManager(DefaultConnectionIdentifier, vendor)
      // We'll do some basic logging of DB activity
      DB.addLogFunc { 
	case (query, time) => {
          Log.info("All queries took " + time + "ms: ")
          query.allEntries.foreach({ case DBLogEntry(stmt, duration) => Log.info(stmt + " took " + duration + "ms")})
          Log.info("End queries")
	}
      }
    }
    // where to search snippet
    LiftRules.addToPackages("net.dataminer")
    Schemifier.schemify(true, Log.infoF _, User, Miner)

    loadInitialData();

    // Build SiteMap
    def sitemap() =  SiteMap(
      Menu(Loc("Home", "index" :: Nil, "Dashboard", User.loginFirst)) ::
      Menu(Loc("User", Nil, "User", PlaceHolder),
           User.sitemap :_*) ::
      Menu(Loc("Miners", "Miners" :: Nil, "Miners", PlaceHolder, User.loginFirst),
	   Menu(Loc("Mine", "miners" :: "mine" :: Nil, "Mine", Hidden, User.loginFirst))
	   :: Miner.menus :_*) ::
      Nil :_*)
    
    LiftRules.statelessRewrite.append {
      case RewriteRequest(
	ParsePath(
	  List("miners", "mine", miner_id),_,_,_),_,_) =>
	    RewriteResponse(List("miners", "mine"), Map("miner_id" -> miner_id))
    }
    
    LiftRules.setSiteMap(sitemap)    

    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    LiftRules.jsArtifacts = JQuery14Artifacts

    LiftRules.early.append(makeUtf8)

    LiftRules.loggedInTest = Full(() => User.loggedIn_?)
          
    S.addAround(DB.buildLoanWrapper)    
  }
  private def makeUtf8(req: HTTPRequest) {
    req.setCharacterEncoding("UTF-8")
  }
  private def loadInitialData() {
    if(User.count == 0)
      User.create.email("admin@test.com").password("123456").validated(true).superUser(true).save
  }
}

