package net.dataminer.snippet

import _root_.net.liftweb.http._
import S._
import _root_.net.liftweb.util._
import Helpers._
import _root_.scala.xml._

class Media {
  private def mediaUrl = if(attr("resource", _.toBoolean) openOr false) "/" + LiftRules.resourceServerPath + "/" else Props.get("media.url", "/media/")
  def js = <script type="text/javascript" src={mediaUrl + attr("src").open_! + ".js"} ></script>
  def css = <link rel="stylesheet" type="text/css" media={attr("media") openOr "screen"} href={mediaUrl + attr("href").open_! + ".css"} />
}
