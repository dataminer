package net.dataminer.snippet

import _root_.net.liftweb.util._
import Helpers._
import _root_.net.liftweb.http._
import _root_.scala.xml._
import _root_.net.liftweb.mapper._
import _root_.net.dataminer.model._

class Miners {
  def latest(in: NodeSeq): NodeSeq = {
    val miners = Miner.findAll(MaxRows(5)).flatMap {miner => 
      bind(
	"entry", chooseTemplate("miner", "entry", in),
	"href" -> SHtml.link("/miners/view/" + miner.id, () => (), Text(miner.name)),
	"user" -> miner.user.obj.map(_.email.toString).open_!)}
    bind("miners", in, "entries" -> miners)
  }    
  def mine(in: NodeSeq): NodeSeq = {    
    val miner = Miner.find(By(Miner.id, S.param("miner_id").open_!.toLong)).open_!
    var start_date = ""
    var end_date = ""
    bind(
      "e", in, 
      "start_date" -> SHtml.text(start_date, start_date = _),
      "end_date" -> SHtml.text(end_date, end_date = _),
      "submit" -> SHtml.submit("Submit", () => Miner.mine(miner, start_date, end_date)))
  }
}
