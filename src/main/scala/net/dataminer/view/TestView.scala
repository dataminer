package net.dataminer.view

import _root_.net.liftweb.http.LiftView
import _root_.scala.xml._
import _root_.javax.script.{ScriptEngineManager, ScriptException}

class TestView extends LiftView {
  override def dispatch = {
    case "index" => index _
  }
  def index(): NodeSeq = {    
    val result = {
      val engine = (new ScriptEngineManager).getEngineByName("python")
      engine.eval("print 'hello jython!'");
    }
    <lift:surround with="default" at="content">
    bla
    </lift:surround>
  }
}

