package net.dataminer.model

import _root_.net.liftweb.mapper._
import _root_.net.liftweb.common._
import _root_.net.liftweb.sitemap._
import _root_.net.liftweb.http._
import _root_.net.liftweb.util._
import Helpers._
import Loc._
import _root_.scala.xml._

trait FixedCRUDify[KeyType, CrudType <: KeyedMapper[KeyType, CrudType] with IdPK] extends CRUDify[KeyType, CrudType] {
  self: CrudType with KeyedMetaMapper[KeyType, CrudType] => 
  override val viewMenuLocParams = Hidden :: super.viewMenuLocParams
  override val editMenuLocParams = Hidden :: super.editMenuLocParams
  override val deleteMenuLocParams = Hidden :: super.deleteMenuLocParams
  override val showAllClass = super.showAllClass + " styled-table"
  override val createClass = super.createClass + " form-table"
  override val _createTemplate = 
    <lift:crud.create form="post">  
      <table id={createId} class={createClass}>  
        <colgroup>
	  <col class="oce-first" />
	</colgroup>
        <crud:field>  
          <tr>  
            <th>  
              <crud:name/>  
            </th>  
            <td>  
              <crud:form/>  
            </td>  
          </tr>  
        </crud:field>     
        <tr>  
          <td> </td>  
          <td><crud:submit>{createButton}</crud:submit></td>  
        </tr>  
      </table>  
    </lift:crud.create>
  override val viewClass = super.viewClass + " form-table"
  override val _viewTemplate = 
    <lift:crud.view>  
      <table id={viewId} class={viewClass}>  
        <crud:row>  
          <tr>  
            <th><crud:name/></th>
            <td><crud:value/></td>
          </tr>
        </crud:row>
        <tr>
          <td colspan="2">
            <crud:actions>
              <crud:action />
            </crud:actions>
          </td>
        </tr>
      </table>        
    </lift:crud.view>  
  override val deleteClass = super.deleteClass + " form-table"
  override val _deleteTemplate = 
    <lift:crud.delete form="post">  
      <table id={deleteId} class={deleteClass}>  
        <crud:field>  
          <tr>  
            <th>  
              <crud:name/>  
            </th>  
            <td>  
              <crud:value/>  
            </td>  
          </tr>  
        </crud:field>  
   
        <tr>  
          <td> </td>  
          <td><crud:submit>{deleteButton}</crud:submit></td>  
        </tr>  
      </table>  
    </lift:crud.delete>  
  override val editClass = super.editClass + " form-table"
  override val _editTemplate = 
    <lift:crud.edit form="post">
      <table id={editId} class={editClass}>
        <crud:field>
          <tr>
            <th>
              <crud:name/>
            </th>
            <td>
              <crud:form/>
            </td>
          </tr>
        </crud:field>

        <tr>
          <td> </td>
          <td><crud:submit>{editButton}</crud:submit></td>
        </tr>
      </table>
    </lift:crud.edit>
  protected val actions: List[Pair[String, String]]
  override def viewMenuLoc: Box[Menu] =
    Full(Menu(new Loc[CrudType]{
      // the name of the page
      def name = "View "+Prefix

      override val snippets: SnippetTest = {
        case ("crud.view", Full(wp)) => displayRecord(wp) _
      }

      def defaultValue = Empty

      def params = viewMenuLocParams

      /**
       * What's the text of the link?
       */
      val text = new Loc.LinkText(calcLinkText _)

      def calcLinkText(in: CrudType): NodeSeq = Text(S.??("crudify.menu.view.displayName", displayName))

      /**
       * Rewrite the request and emit the type-safe parameter
       */
      override val rewrite: LocRewrite =
        Full(NamedPF(name) {
          case RewriteRequest(pp , _, _)
          if pp.wholePath.startsWith(viewPath) &&
          pp.wholePath.length == (viewPath.length + 1) &&
          findForParam(pp.wholePath.last).isDefined
          =>
            (RewriteResponse(viewPath),findForParam(pp.wholePath.last).open_!)
        })

      def displayRecord(entry: CrudType)(in: NodeSeq): NodeSeq = {
        def doRow(in: NodeSeq): NodeSeq =
          entry.formFields.flatMap(
            f => bind("crud", in, "name" -> f.displayHtml, "value" -> f.asHtml)
          )
        def doAction(in: NodeSeq): NodeSeq =
          actions.flatMap{ case (href, label) =>
            bind("crud", in, "action" -> SHtml.link(href + "/" + entry.id, () => "", Text(label)))}
        bind(
          "crud", in, 
          "row" -> doRow _,
          "actions" -> doAction _
           )
      }

      override def calcTemplate = Full(viewTemplate)

      val link =
        new Loc.Link[CrudType](viewPath, false) {
          override def createLink(in: CrudType) =
            Full(Text(viewPathString+"/"+obscurePrimaryKey(in)))
        }
    }))
}
