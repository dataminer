package net.dataminer.model

import _root_.net.liftweb.http._
import _root_.net.liftweb.mapper._
import _root_.net.liftweb.util._
import _root_.net.liftweb.common._

class User extends MegaProtoUser[User] {
  def getSingleton = User // what's the "meta" server  
}

object User extends User with MetaMegaProtoUser[User] {
  type ModelType = User
  override def dbTableName = "users"
  override def screenWrap = Full(
    <lift:surround with="default" at="content">
    <lift:bind />
    </lift:surround>)  
  autologinFunc = if (Props.devMode) Full(() =>
    {User.find(1).foreach(User.logUserIn)}) else Empty
  override def editXhtml(user: ModelType) = {
    (<form method="post" action={S.uri}>
        <table class="form-table"><tr><th colspan="2">{S.??("edit")}</th></tr>
          {localForm(user, true)}
          <tr><td> </td><td><user:submit/></td></tr>
        </table>
     </form>)
  }
  override def changePasswordXhtml = {
    (<form method="post" action={S.uri}>
        <table class="form-table"><tr><th colspan="2">{S.??("change.password")}</th></tr>
          <tr><td>{S.??("old.password")}</td><td><user:old_pwd /></td></tr>
          <tr><td>{S.??("new.password")}</td><td><user:new_pwd /></td></tr>
          <tr><td>{S.??("repeat.password")}</td><td><user:new_pwd /></td></tr>
          <tr><td> </td><td><user:submit /></td></tr>
        </table>
     </form>)
  }
  override def loginXhtml = {
    (<form method="post" action={S.uri}><table class="form-table"><tr><th
              colspan="2">{S.??("log.in")}</th></tr>
          <tr><td>{S.??("email.address")}</td><td><user:email /></td></tr>
          <tr><td>{S.??("password")}</td><td><user:password /></td></tr>
          <tr><td><a href={lostPasswordPath.mkString("/", "/", "")}
                >{S.??("recover.password")}</a></td><td><user:submit /></td></tr></table>
     </form>)
  }
  override def signupXhtml(user: ModelType) = {
    (<form method="post" action={S.uri}><table class="form-table"><tr><th
              colspan="2">{ S.??("sign.up") }</th></tr>
          {localForm(user, false)}
          <tr><td> </td><td><user:submit/></td></tr>
                                        </table></form>)
  }
  override def lostPasswordXhtml = {
    (<form method="post" action={S.uri}>
        <table class="form-table"><tr><th
              colspan="2">{S.??("enter.email")}</th></tr>
          <tr><td>{S.??("email.address")}</td><td><user:email /></td></tr>
          <tr><td> </td><td><user:submit /></td></tr>
        </table>
     </form>)
  }
}

