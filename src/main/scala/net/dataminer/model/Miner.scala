package net.dataminer.model

import _root_.net.liftweb.http._
import S._
import _root_.net.liftweb.util._
import Helpers._
import _root_.scala.xml._

import _root_.net.liftweb.mapper._
import _root_.net.liftweb.common._
class Miner extends LongKeyedMapper[Miner] with IdPK {
  def getSingleton = Miner
  object name extends MappedString(this, 50)
  object url_template extends MappedString(this, 255)
  object override_function extends MappedText(this)
  object user extends MappedLongForeignKey(this, User) {
    override def asHtml = Text(this.obj.map(_.email.toString).open_!)
    override def dbIncludeInForm_? = false
  }
}
object Miner extends Miner with LongKeyedMetaMapper[Miner] with FixedCRUDify[Long, Miner] {
  def mine(entry: Miner, start_date: String, end_date: String) {
    
  }
  override def dbTableName = "miners"
  override def fieldOrder = id :: name :: url_template :: override_function :: user :: Nil
  override def fieldsForList = name :: url_template :: override_function :: user :: Nil  
  def setCurrentUser(miner: Miner) { miner.user(User.currentUser.open_!) }
  override val beforeSave = setCurrentUser _ :: super.beforeSave 
  override val actions = List(
    "/miners/mine" -> "Mine")  
}
